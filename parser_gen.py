''' Author          : Sarah Bronwen Currie
    Date created    : 18/07/21
'''

import re

def _match(regex, string, cur_pos):
    '''
    Tries to match the regex in the string at the given position (removing leading whitespace)

    Arguments:
        regex   : the regular expression to be matched in the string
        string  : the string that might contain a section matching the regex
        cur_pos : the current position in string

    Returns:
        the string of the regex matched (None if not found)
        the length of the regex matched (None if not found)
    '''
    
    ws = re.match(r'\s*', string[cur_pos:])
    length = len(ws.group(0))

    token = re.match(regex, string[cur_pos+length:], re.VERBOSE|re.DOTALL)
    if token is None:
        return None,None
    matched = token.group(0)
    return matched, len(matched)+length


class _Error(Exception):
    '''
    Base class for exceptions

    Error(msg, string, index)
    Args:
        msg    : string containing the error message
        string : string containing the grammar for parser generation
                 (may by None)
        index   : position into string
                 (may be None)
    '''
    def __init__(self, msg, string=None, index=None):
        self.msg = msg
        self.string = string
        self.index = index        

    def __str__(self):
        if (self.index is not None) and (self.string is not None):
            self._position()
            return "{} at line {}, column {}: {}".format(self.__class__.__name__[1:], self.line, self.col, self.msg)
        return "{}: {}".format(self.__class__.__name__[1:], self.msg)
    
    def _position(self):
        ''' find line and column numbers for index in string'''
        self.line = self.string.count('\n', 0, self.index)+1
        self.col = self.index - self.string.rfind('\n', 0, self.index)

class _GrammarError(_Error):
    ''' Raised when there is an error in the string defining the grammar'''
    pass

class _ParserError(_Error):
    ''' Raised when there is an error in parsing the input using the generated parser'''
    pass

 
class _Parser:
    '''
    Base class for all parser expressions

    Each subclass has a parse() method:
        Tries to parse the string at the current index

        Arguments:
            string  : the input string to be parsed
            index   : the current index in the string
            rules   : a dict of all rules in the generated grammar
        
        Returns:
            the updated index

        Raises:
            ParserError if unable to parse string at current index
            GrammarError if an error in the grammar prevents parsing
            
    '''
    def __call__(self, string, index, rules):
        return self._parse(string, index, rules)

class _Regex(_Parser):
    '''
    Parser for regular expressions

    Arguments:
        regex   : string containing regular expression
    '''
    def __init__(self, regex):
        self.regex = regex
    
    def _parse(self, string, index, rules):
        token, length = _match(self.regex, string, index)
        if token is None:
            raise _ParserError("Expected a token matching the regex: '{}'".format(self.regex), string, index)
        return index+length

class _Ident(_Parser):
    '''
    Parser for an identifier in the grammar
    
    Arguments:
        name    : string containing the name of the identifier
    '''
    def __init__(self, name):
        self.name = name
    
    def _parse(self, string, index, rules):        
        try:
            # find rule from the rules dict, then parse
            is_axiom, expr = rules[self.name]
            cur_index = expr(string, index, rules)
            return cur_index
        except KeyError:
            raise _GrammarError("Identifier {} is not defined".format(self.name))

class _Rep(_Parser):
    '''
    Parser for repetition in the grammar
    
    Arguments:
        parser  : the parser that it repeated
        min     : the minimum number of times the parser can be repeated
        max     : the maximum number of times the parser can be repeated
    '''
    def __init__(self, parser, min, max, expr):
        self.parser = parser
        self.min = min
        self.max = max
        self.expr = expr
    
    def _parse(self, string, index, rules):
        count = 0
        cur_index = index
        while (count != self.max):
            try:
                cur_index = self.parser(string, cur_index, rules)
            except (_GrammarError, _ParserError):
                if (count < self.min):
                    raise _ParserError("Less than the expected number of repetitions when parsing '"+self.expr+"' from the grammar", string, index)
                break
            count += 1
        return cur_index

class _And(_Parser):
    '''
    Parser for and-expressions

    Arguments:
        parsers : list of parsers in the and-expression
    '''
    def __init__(self, parsers, expr):
        self.expr = expr
        self.parsers = []
        for p in parsers:
            if isinstance(p, _And):
                self.parsers.extend(p.parsers)
            else:
                self.parsers.append(p)
                
    def _parse(self, string, index, rules):
        cur_index = index
        try:
            for p in self.parsers:
                cur_index = p(string, cur_index, rules)
        except (_GrammarError, _ParserError):
            raise _ParserError("Failed finding the expected input when parsing '"+self.expr+"' from the grammar", string, index)
        return cur_index

class _Or(_Parser):
    '''
    Parser for or-expressions

    Arguments:
        parsers : list of parsers in the or-expression
    '''
    def __init__(self, parsers, expr):
        self.expr = expr
        self.parsers = []
        for p in parsers:
            if isinstance(p, _Or):
                self.parsers.extend(p.parsers)
            else:
                self.parsers.append(p)

    def _parse(self, string, index, rules):
        # check each option, return longest match
        longest = -1
        cur_index = index
        for p in self.parsers:
            try:
                cur_index = p(string, index, rules)
                if (cur_index > longest):
                    longest = cur_index
            except (_ParserError, _GrammarError):
                pass            

        if (longest > -1):
            return longest
        else:
            raise _ParserError("Failed finding an expected input when parsing '"+self.expr+"' from the grammar", string, index)

class _Rules(_Parser):
    '''
    Parser for the grammar

    Arguments:
        rules   : dictionary of rules in the grammar
    '''
    def __init__(self, rules):
        self.rules = rules
    
    def _parse(self, string):
        try:
            # find axiom and call parse() on it with input string and index=0
            for ident,(is_axiom,expr) in self.rules.items():
                if is_axiom:
                    try:
                        cur_index = expr(string, 0, self.rules)
                    except (_ParserError, _GrammarError) as err:
                        raise err
            if (cur_index != len(string)):
                raise _ParserError("Unable to parse input", string, cur_index)
            print("Successfully parsed")

        except (_GrammarError, _ParserError) as err:
            print(err)

class ParserGen:
    ''' 
    A class to represent a parser made from a string containing the grammar 
    
    Arguments:
        input_grammar   : string containing the definition of the grammar
    '''
    def __init__(self, input_grammar):
        ''' Generates a parser from input_grammar'''
        self.input_grammar = input_grammar
        self.rules = {}
        self.index = 0

        try:
            self._grammar_start()
            # ensure only 1 axiom
            axioms = 0
            for ident,(is_axiom,rule) in self.rules.items():
                if is_axiom:
                    axioms+=1
            if (axioms == 0):
                raise _GrammarError("Grammar needs at least 1 axiom (a rule beginning with 'START')")
            elif (axioms > 1):
                raise _GrammarError("Too many axioms - grammar can only have 1 axiom")
            # create parser object to contain dict of self.rules
            self.parser = _Rules(self.rules)
        except _GrammarError as err:
            print()
            print(err)
            print("Unable to generate parser. Quitting...\n")
            quit()

    def __call__(self, string):
        '''Parses input string with generated parser'''
        self.parser._parse(string)

    def _grammar_start(self):
        ''' 
        Parses grammar rules by continuously matching rules until end of grammar string
        
        Grammar rule: 
            grammar_start = rule* 
        '''
        # end of string represented by '\Z'
        token, length = _match(r'\Z', self.input_grammar, self.index)
        while token is None:
            self._rule()
            token, length = _match(r'\Z', self.input_grammar, self.index)
        self.index += length

    def _rule(self):
        ''' 
        Parses a rule, stores the matched expression in self.rules under its identifier
        
        Grammar rule: 
            rule = 'START' ident '=' expr_or | ident '=' expr_or ';' 
            - optional 'START' at beginning of rule signifies axiom

        Raises:
            GrammarError if grammar rule not found in grammar string
        '''
        # check whether axiom        
        is_axiom = False
        token, length = _match(r'START', self.input_grammar, self.index)
        if token:
            is_axiom = True
            self.index += length
        
        ident = self._ident()

        token, length = _match(r'=', self.input_grammar, self.index)
        if token is None:
            raise _GrammarError("Expected a '=' symbol as part of a rule: identifier = expression;", self.input_grammar, self.index)
        self.index += length

        # add tuple(isaxiom, expression) to self.rules dict under identifier name
        self.rules[ident.name] = (is_axiom, self._expr_or())
        
        token, length = _match(r';', self.input_grammar, self.index)
        if token is None:
            raise _GrammarError("Expected a ';' symbol as part of a rule: identifier = expression;", self.input_grammar, self.index)
        self.index += length

    def _expr_or(self):
        '''
        Parses an or-expression

        Grammar rule:
            expr_or = expr_and ('|' expr_and)* 

        Returns: 
            an Or object containing list of Parser objects,
            or a single Parser object
        
        Raises: 
            GrammarError if no expression after an or symbol '|'
        '''
        start = self.index
        and_parsers = []
        and_parsers.append(self._expr_and())

        # continue calling expr_and() if '|' is inbetween them
        token, length = _match(r'\|', self.input_grammar, self.index)
        while token:
            self.index += length
            try:             
                and_parsers.append(self._expr_and())               
            except _GrammarError:
                raise _GrammarError("Invalid expression after '|' symbol", self.input_grammar, self.index)
            token, length = _match(r'\|', self.input_grammar, self.index)
        end = self.index

        if len(and_parsers) == 1:
            return and_parsers[0]
        return _Or(and_parsers, self.input_grammar[start:end])

    def _expr_and(self):
        '''
        Parses an and-expression
        
        Grammar rule:
            expr_and = expr_rep (expr_rep)* 

        Returns: 
            an And object containing a list of Parser objects,
            or a single Parser object
        '''
        start = self.index
        rep_parsers = []
        rep_parsers.append(self._expr_rep())

        while True:
            try:  
                rep_parsers.append(self._expr_rep())
            except _GrammarError:
                break       
        end = self.index

        if len(rep_parsers)==1:
            return rep_parsers[0]
        return _And(rep_parsers, self.input_grammar[start:end])        

    def _expr_rep(self):
        '''
        Parses a repetition

        Grammar rule:
            expr_rep = symbol'*' | symbol'+' | symbol'?' | symbol 

        Returns: 
            a Rep object containing the Parser matching the symbol and the min and max number of repetitions
        '''
        min = None
        max = None
        start = self.index
        parser = self._symbol()

        # match '*' for 0 to infinite repetitions
        token, length = _match(r'\*', self.input_grammar, self.index)
        if token:
            self.index += length
            min = 0
            max = -1

        # '+' for 1 to infinite repetitions
        token, length = _match(r'\+', self.input_grammar, self.index)
        if token:
            self.index += length
            min = 1
            max = -1

         # '?' for 0 or 1 repetitions
        token, length = _match(r'\?', self.input_grammar, self.index)
        if token:
            self.index += length
            min = 0
            max = 1
        
        end = self.index        
        if (max is not None and min is not None):
            return _Rep(parser, min, max, self.input_grammar[start:end])
        return parser

    def _symbol(self):
        ''' 
        Parses a symbol in the grammar

        Grammar rule:
            symbol = '(' expr_or ')' | regex | ident

        Returns: 
            a Parser object 

        Raises: 
            GrammarError if no symbol found or when closing bracket around expr_or not found
        '''        
        token, length = _match(r'\(', self.input_grammar, self.index)
        if token:
            self.index += length
            parser = self._expr_or()
            token, length = _match(r'\)', self.input_grammar, self.index)
            if token is None:
                raise _GrammarError("Expected matching closing bracket ')' after expression", self.input_grammar, self.index)
            self.index += length
        else:
            try:
                parser = self._regexpr()
            except _GrammarError:
                try:
                    parser = self._ident()
                except _GrammarError:
                    raise _GrammarError("Expected regular expression, identifier or an expression in brackets", self.input_grammar, self.index)
        return parser

    def _ident(self):
        '''
        Parses an identifier in the grammar, matches an underscore or letter followed by any number of letters, underscores and digits

        Grammar rule in python regex:
            ident = [_a-zA-Z][_a-zA-Z0-9]*
        
        Returns: 
            an Ident object containing the name of the identifier
        
        Raises: 
            GrammarError if can't match a valid string
        '''
        regex = r'[_a-zA-Z][_a-zA-Z0-9]*'
        token, length = _match(regex, self.input_grammar, self.index)
        if token is None:
            raise _GrammarError("Expected a valid identifier (an underscore or letter followed by any number of letters, underscores and digits)",
                                 self.input_grammar, self.index)
        self.index += length
        return _Ident(token)

    def _regexpr(self):
        ''' 
        Parses a regular expression in the grammar, matches anything in string

        Grammar rule in python regex: 
            regex = "{3}.*?"{3} |".*?" | '{3}.*?'{3} | '.*?'

        Returns: 
            a Regex object containing the regex matched inside the string

        Raises: 
            GrammarError if cannot match string
        '''
        regex = r''' "{3}.*?"{3} |".*?" | '{3}.*?'{3} | '.*?'  '''
        string, length = _match(regex,self.input_grammar,self.index)
        if string is None:
            raise _GrammarError("Expected regular expression within quotation marks", self.input_grammar, self.index)
        self.index += length

        # remove the opening and closing quotes of the regex since not part of the regex
        if string[:3]=='"""' and string[-3:]=='"""':
            token = string[3:-3]
        elif string[:3]=="'''" and string[-3:]=="'''":
            token = string[3:-3]
        elif string[:1]=='"' and string[-1:]=='"':
            token = string[1:-1]
        elif string[:1]=="'" and string[-1:]=="'":
            token = string[1:-1]

        return _Regex(token)
