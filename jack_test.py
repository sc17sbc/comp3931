from parser_gen import *

input_grammar = open("jack_grammar.txt", "r")
grammar = input_grammar.read()
parser = ParserGen(grammar) 

input_file = open("Square.jack", "r")
inp = input_file.read()
parser(inp)
