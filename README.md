# Parser Generator Tool
This parser generator tool generates a recursive decent parser from the input grammar.

## Requirements
- Python 3.9

## Usage
- Create a text file containing the grammar definition
- Create a python file similiar to the following:

```python
from parser_gen import *

# read in the grammar file
f = open("grammar.txt", "r")
grammar = f.read()

# initialise ParserGen on the grammar string
parser = ParserGen(grammar) 

# call the ParserGen object on the input string
expr = input()
parser(expr)
```

- Ensure the grammar file, the python file and parser_gen.py are in the same directory
- Run the created python file from the command line (within the directory containing it):

```bash
python python_file.py
```

