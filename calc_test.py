from parser_gen import *

input_file = open("calc_grammar.txt", "r")
input_grammar = input_file.read()

parser = ParserGen(input_grammar) 

while True:
    expr = input(": ")
    parser(expr)